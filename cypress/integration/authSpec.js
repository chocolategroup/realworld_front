describe("The User", function() {
  it("can successfully login and then log out", function() {
    cy.visit("/");
    cy.get(":nth-child(2) > .nav-link").click();
    cy.get(":nth-child(1) > .form-control").type(
      "paradigmatico@paradigmadigital.com"
    );
    cy.get(":nth-child(2) > .form-control").type("12345678");
    cy.get(".btn").click();
    cy.visit("/settings");
    cy.get('.btn-outline-danger').click();
  });
});
