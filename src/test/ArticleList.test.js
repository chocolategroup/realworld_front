import React from "react";
import { create } from "react-test-renderer";
import { mount } from "../../enzyme";
import ArticleList from "../components/ArticleList";

describe("Article List", () => {
  test("it renders the component", () => {
    const component = create(<ArticleList />);
    expect(component).toMatchSnapshot();
  });
  test("it renders `Loading...` screen when there are no props", () => {
    const component = mount(<ArticleList />);
    expect(component.find("div").text()).toEqual("Loading...");
  });
});
